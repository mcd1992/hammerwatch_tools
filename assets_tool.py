#!/usr/bin/env python
import sys, os
from optparse import OptionParser, OptionGroup
from hammerwatch_tools import HWRA_File


# Setup optparse
optParser = OptionParser( usage="%prog [options] <target file or directory>" )
unpackGroup = OptionGroup( optParser, "Unpackaging Options", "Use these options to extract an assets.bin into a directory." )
repackGroup = OptionGroup( optParser, "Repackaging Options", "Use these options to repackage a directory into an assets.bin" )


# Unpacking group options
unpackGroup.add_option( "-l", "--list-assets", dest="listAssets", action="store_true", default=False, help="List all the assets in this archive." )
unpackGroup.add_option( "-u", "--unpack-file", dest="unpackFile", help="Unpackage this single file from the archive into the current directory.", metavar="FILENAME" )
unpackGroup.add_option( "-x", "--unpack-all", dest="unpackAll", action="store_true", default=False, help="Unpackage the entire archive into the assets/ directory [unless -d option specified]." )
unpackGroup.add_option( "-d", "--directory", dest="unpackDir", default="assets/", help="Extract into this directory instead of assets/.", metavar="DIRECTORY" )


#Repacking group options
repackGroup.add_option( "-r", "--repack", dest="repackDir", action="store_true", default=False, help="Repackage target directory into a asset.bin formatted archive..", metavar="DIRECTORY" )
repackGroup.add_option( "-f", "--file", dest="repackFile", default="modified_assets.bin", help="Target archive filename [default is modified_assets.bin].", metavar="FILE" )


# Start parsing arguments
optParser.add_option_group( unpackGroup )
optParser.add_option_group( repackGroup )
(options, args) = optParser.parse_args()

lastStdoutLen = 0 # This will keep track of the last printed strings length for clearing later
slashChar = "\\" if (os.name == "nt") else "/" # Stupid windows and their \


if( len( args ) < 1 ):
    print( "You must specify the archive file or directory as the last argument." )
    print( " specify the -h or --help option for usage." )
    sys.exit( 1 )


# Helper function definitions
def WriteAsset( filePath, bData ):
    try:
        path = os.path.dirname( filePath )
        os.makedirs( path )

    except:
        pass

    f = open( filePath, "wb" )
    f.write( bData )
    f.close()


def PrintAsset( info ):
    global lastStdoutLen # Uhh I'm not sure
    outstring = "\r Progress: [%(current)i/%(max)i] Saving file: %(currentFile)s" % info
    sys.stdout.write( outstring + (" " * (lastStdoutLen-len(outstring))) )
    sys.stdout.flush()
    lastStdoutLen = len( outstring )


# Options parsing
if( options.unpackFile or options.unpackAll or options.listAssets ):
    try:
        hwraFile = open( args[0], "rb" )
    except OSError:
        print( "Failed to open the target archive: " + args[0] )
        sys.exit( 1 )


    hwra = HWRA_File( hwraFile )
    if( hwra.IsValid() ):
        if( options.unpackAll ):
            if( not slashChar in options.unpackDir ):
                dirPrefix = options.unpackDir + slashChar
            else:
                dirPrefix = options.unpackDir

            print( "Unpacking assets from " + args[0] + " in to " + options.unpackDir + " directory..." )
            lastStdoutLen = 0
            assetList = hwra.GetAssetsList() # Write all the assets
            for fileName in assetList:
                asset = hwra.GetAsset( fileName )
                if( asset ):
                    outstring = "\r Writing File: " + fileName
                    sys.stdout.write( outstring + (" " * (lastStdoutLen-len(outstring))) )
                    sys.stdout.flush()
                    WriteAsset( dirPrefix + fileName, asset.GetData() )
                    lastStdoutLen = len( outstring )
            sys.stdout.write( "\r DONE" + (" " * lastStdoutLen) + "\n" )
            sys.stdout.flush()


        elif( options.unpackFile ): # Save a single asset
            asset = hwra.GetAsset( options.unpackFile )
            if( asset ):
                WriteAsset( os.path.basename( options.unpackFile ), asset.GetData() )


        elif( options.listAssets ): # Print all the assets
            for fileName in hwra.GetAssetsList():
                print( fileName )

    else:
        print( args[0] + " is not a valid HWRA file." )
    hwra.Close()


elif( options.repackDir ):
    if( args[0][-1] == slashChar ): # Remove trailing slash character
        args[0] = args[0][:-1]

    if( not os.path.exists( args[0] ) ):
        print( "Failed to find target directory: " + args[0] )
        sys.exit( 1 )

    hwra = HWRA_File.CreateNew( options.repackFile ) # Create new/empty archive.
    if( hwra.IsValid() ):
        print( "Building assets in " + args[0] + " directory..." )
        lastStdoutLen = 0
        for root, dirs, files in os.walk( args[0] ):
            for fileName in files:
                filePath = root + slashChar + fileName
                if( root != args[0] ):
                    # The HWRA format uses / internally regardless of platform. Replace \ with / and strip the prefix directory
                    assetName = root.replace( args[0] + slashChar, "" ).replace( "\\", "/" ) + "/" + fileName

                else:
                    assetName = fileName

                #print( filePath + " : " + assetName )
                try:
                    data = open( filePath, "rb" )
                except:
                    print( "Failed to open asset: " + filePath )
                    sys.exit( 1 )

                outstring = "\r" + " Building asset: " + assetName
                sys.stdout.write( outstring + (" " * (lastStdoutLen-len(outstring))) ) # Write our string and any extra spaces needed to cover up the previous string
                sys.stdout.flush()
                hwra.AddAsset( assetName, data.read() )
                data.close()
                lastStdoutLen = len( outstring )

        sys.stdout.write( "\r DONE" + (" " * lastStdoutLen) + "\n" )
        sys.stdout.flush()

        print( "Saving assets found in " + args[0] + " directory into " + options.repackFile + " [This may take a while...]" )
        hwra.SaveChanges( PrintAsset )
        sys.stdout.write( "\r DONE" + (" " * lastStdoutLen) + "\n" )

    hwra.Close()
