import struct, io
from .hwra_asset import HWRA_Asset

class HWRA_File:
    def __init__( self, fileHandle, createNew=False ):
        if( fileHandle.closed ):
            self._isValid = False
            return

        self._fileHandle = fileHandle # Save the filehandle
        if( fileHandle.tell() != 0 ): # Seek to the beginning of the file if needed.
            fileHandle.seek( 0 )

        magic = fileHandle.read( 4 ) # First 4 bytes are the 'magic' HWRA
        if( magic.decode( "utf-8" ) == "HWRA" ):
            self._isValid = True
        else:
            self._isValid = False # This isn't a valid HWRA file
            return

        bNumFiles = fileHandle.read( 4 ) # Next 4 bytes are the total number of files in the HWRA file
        numFiles = struct.unpack( "I", bNumFiles )[0]
        
        self._assetsList = {} # Dictionary that will contain all the files in the binary archive

        for i in range( 0, numFiles ):
            try:
                bFileNameSize = fileHandle.read( 1 ) # Next byte is the size of the filename (following is a string with no null terminator)
                fileNameSize = struct.unpack( "B", bFileNameSize )[0]

                bFileName = fileHandle.read( fileNameSize ) # Read X bytes which is the filename with no null terminator
                fileName = bFileName.decode( "utf-8" )

                bDataSize = fileHandle.read( 4 ) # Next 4 bytes is the size of the data belonging to filename
                dataSize = struct.unpack( "I", bDataSize )[0]

                dataStartOffset = fileHandle.tell()
                bData = fileHandle.read( dataSize )
                dataEndOffset = fileHandle.tell()

                self._assetsList[ fileName ] = HWRA_Asset( dataStartOffset, bytearray( bData ) )

            except: # Set object as invalid and break if we encounter any error while parsing the archive.
                self._isValid = False
                break

        if( createNew ):
            self._isValid = True # True if we're creating a new hwra file
        self._assetDeleted = False


    @classmethod
    def CreateNew( cls, fileName ):
        fileHandle = open( fileName, "w+b" )
        fileHandle.write( b"HWRA" + struct.pack( "I", 0 ) ) # Write magic and number of files.
        return cls( fileHandle, True )


    # Returns false if any errors are encountered
    def IsValid( self ):
        return self._isValid


    # Returns true if any of this archives assets have been modified.
    def HasChanges( self ):
        if( self._assetDeleted ):
            return True

        for name in self._assetsList:
            asset = self._assetsList[ name ]
            if( asset and asset.HasChanges() ):
                return True
        return False


    # Save any changes made to this archives assets, if any.
    def SaveChanges( self, progressCallback=None ):
        if( self.HasChanges() and self._isValid and self._fileHandle and not self._fileHandle.closed ):
            fileName = self._fileHandle.name # Save the filename
            oldMode = self._fileHandle.mode # Save the old file mode
            oldCursor = self._fileHandle.tell()

            if( oldMode != "w+b" ): # Close and re-open the file handle if it's not already in readwrite mode
                self._fileHandle.close() # Close the current filehandle since it's probably readonly
                self._fileHandle = open( fileName, "w+b" ) # Re-open file for read and write
                self._fileHandle.write( self.GetRebuiltData( progressCallback ) ) # Write the new contents to the file
                self._fileHandle.close() # Close the readwrite file now that we've saved the data
                self._fileHandle = open( fileName, oldMode ) # Re-open with the previous filemode
                self._fileHandle.seek( oldCursor )
            else:
                self._fileHandle.seek( 0 )
                self._fileHandle.write( self.GetRebuiltData( progressCallback ) )
                self._fileHandle.seek( oldCursor )

            
            for name in self._assetsList: # Mark all assets as unchanged
                asset = self._assetsList[ name ]
                asset.SetChanged( False )
            self._assetDeleted = False

        return None


    # Returns a tuple of all the assets/filenames in this hwra file.
    def GetAssetsList( self ):
        if( self._isValid ):
            assetList = []
            for name in self._assetsList:
                assetList.append( name )
            return assetList
        return None


    # Returns an HWRA_Asset object that can be modified or read.
    def GetAsset( self, fileName ):
        if( self._isValid and (fileName in self._assetsList) ):
            asset = self._assetsList[ fileName ]
            if( asset.IsValid() ):
                return asset
        return None


    # Returns a re-built assets.bin formatted bytearray that can be written and properly used by Hammerwatch.
    def GetRebuiltData( self, progressCallback=None ):
        if( self.IsValid() ):
            data = b"HWRA" + struct.pack( "I", len( self._assetsList ) ) # Create magic and number of files header as the first part of the hwra data.

            self._rebuildProgress = { "current": 1, "max": len( self._assetsList ), "currentFile": "" } # Create a dictionary attribute for storing current rebuild progress
            for fileName in self._assetsList:
                self._rebuildProgress["currentFile"] = fileName

                if( callable( progressCallback ) ):
                    progressCallback( self._rebuildProgress )

                asset = self._assetsList[ fileName ]
                data += struct.pack( "B", len( fileName ) )  # Write the size of this assets filename
                data += fileName.encode( "utf-8" )  # Write the filename
                data += struct.pack( "I", asset.GetSize() ) # Write the filesize
                data += asset.GetData()
                self._rebuildProgress["current"] += 1

            self._rebuildProgress = None
            return data # Return the whole file with modifications

        return None


    def DeleteAsset( self, fileName ):
        if( fileName in self._assetsList ):
            self._assetDeleted = True
            return self._assetsList.pop( fileName, None )
        return None


    def AddAsset( self, fileName, data ):
        if( fileName in self._assetsList ):
            return None # Asset already exists

        newAsset = HWRA_Asset( None, data )
        newAsset.SetChanged( True )
        self._assetsList[ fileName ] = newAsset
        return newAsset


    # Close the filehandle and decrement this objects reference count.
    def Close( self ):
        self._isValid = False
        self._assetsList = {}

        if( self._fileHandle ):
            self._fileHandle.close()
        del self
