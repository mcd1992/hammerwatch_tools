class HWRA_Asset:
    def __init__( self, start, assetData ):
        self._isValid = False

        if( start ):
            self._start = start
        else:
            self._start = 0

        if( isinstance( assetData, bytearray ) ):
            self._assetData = assetData
        else:
            self._assetData = bytearray( assetData )

        self._assetModified = False # True if this assets changes needs to be saved to disk.

        self._size = len( assetData )
        self._end = self._start + self._size
        self._isValid = True


    def SetChanged( self, isChanged ):
        if( isChanged and isinstance( isChanged, bool ) ):
            self._assetModified = isChanged

        self._assetModified = True


    def HasChanges( self ):
        return self._assetModified


    def IsValid( self ):
        return self._isValid


    def GetStart( self ):
        return self._start


    def GetSize( self ):
        return self._size


    def GetEnd( self ):
        return self._end


    def GetData( self ):
        return self._assetData


    def SetData( self, newData ):
        if( isinstance( newData, bytearray ) ):
            self._assetData = newData
        else:
            self._assetData = bytearray( newData )

        self._assetModified = True
        self._size = len( newData )
        self._end = self._start + self._size
