hammerwatch_tools
=================

Open source and cross-platform python library for assisting in the modification of hammerwatch's binary file formats.
For now the library only supports HWRA formatted files, like assets.bin, but support for the other formats is in progress.
For more information be sure to visit the wiki. https://github.com/mcd1992/hammerwatch_tools/wiki
