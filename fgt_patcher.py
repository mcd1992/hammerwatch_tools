#!/usr/bin/env python

import sys
from optparse import OptionParser
from hammerwatch_tools import HWRA_File


optParse = OptionParser( usage="%prog [options] <patch.bin>" )
optParse.add_option( "-t", "--target", dest="targetFile", default="assets.bin", help="Target file to apply the patches to. [Default: assets.bin]", metavar="FILE" )
optParse.add_option( "-v", "--verbose", dest="verbose", action="store_true", default=False, help="Print debug information." )


(options, args) = optParse.parse_args()


lastStdoutLen = 0
def PrintStatus( info ):
    global lastStdoutLen
    outstring = "\r Progress: [%(current)i/%(max)i] Saving file: %(currentFile)s" % info
    sys.stdout.write( outstring + (" " * (lastStdoutLen-len(outstring))) )
    sys.stdout.flush()
    lastStdoutLen = len( outstring )


if( len( args ) < 1 ):
    print( "You must specify the target patch file, to read the changes from, as the last argument." )
    print( "For usage and help run this program with the --help option." )
    sys.exit( 1 )

if( options.targetFile and args[0] ):
    try:
        patchFile = open( args[0], "rb" )
    except:
        print( "Failed to open " + args[0] + " for reading." )
        sys.exit( 1 )


    try:
        targetFile = open( options.targetFile, "r+b" )
    except:
        print( "Failed to open " + options.targetFile + " for modification." )
        patchFile.close()
        sys.exit( 1 )


    patchFile = HWRA_File( patchFile )
    targetFile = HWRA_File( targetFile )
    if( patchFile.IsValid() and targetFile.IsValid() ):
        sys.stdout.write( "Applying patches to " + options.targetFile + " please wait... " )
        if( options.verbose ):
            print()

        for fileName in patchFile.GetAssetsList():
            asset = targetFile.GetAsset( fileName )
            if( asset and asset.IsValid() ):
                if( options.verbose ): print( " Patching " + fileName + " in " + options.targetFile )
                asset.SetData( patchFile.GetAsset( fileName ).GetData() )

            else:
                if( options.verbose ): print( " Adding new asset, " + fileName + ", into " + options.targetFile )
                targetFile.AddAsset( fileName, patchFile.GetAsset( fileName ).GetData() )

        print( "DONE" )
        print( "Writing changes to " + options.targetFile + " [This may take a while...]" )
        targetFile.SaveChanges( PrintStatus )
        sys.stdout.write( "\r DONE" + (" " * lastStdoutLen) + "\n" )
        


    else:
        if( not patchFile.IsValid() ):
            print( args[0] + " is not a valid hammerwatch assets file." )
        if( not targetFile.IsValid() ):
            print( options.targetFile + " is not a valid hammerwatch assets file." )
    patchFile.Close()
    targetFile.Close()
